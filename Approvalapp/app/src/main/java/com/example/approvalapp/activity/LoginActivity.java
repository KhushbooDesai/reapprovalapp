package com.example.approvalapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.approvalapp.Interface.Api;
import com.example.approvalapp.Model.LoginResponse;
import com.example.approvalapp.R;
import com.example.approvalapp.helper.NetworkManager;
import com.example.approvalapp.helper.RetrofitAPIClient;
import com.example.approvalapp.helper.SharedPref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity  implements View.OnClickListener{

    private LoginActivity activity;
    private EditText etpsno, etpassword;
    private Button btnlogin;
    private CheckBox checkpass;
    private ProgressDialog progressDialog;

    //api inside the interface folder
    private Api api;
    private SharedPref sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sharedPref = new SharedPref(LoginActivity.this);
        initView();
        initViewAction();
        initViewListener();
        //usernm= sharedPref.getString("userPsno");
      //  Log.e("username is",usernm);
       // userPsno.setText(usernm.toString());


    }
    private void initView() {
        etpsno = findViewById(R.id.etpsno);
        etpassword = findViewById(R.id.etpassword);
        btnlogin = findViewById(R.id.btnlogin);
        checkpass = (CheckBox) findViewById(R.id.checkpass);


    }

    private void initViewAction() {

    }

    private void initViewListener() {
        btnlogin.setOnClickListener(this);
        checkpass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    etpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    etpassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnlogin:
                if (NetworkManager.isWifiConnected(LoginActivity.this) || NetworkManager.isInternetConnected(LoginActivity.this)) {
                    if ((etpsno.getText().toString().equalsIgnoreCase("")) || (etpassword.getText().toString().equalsIgnoreCase(""))) {
                        Toast.makeText(LoginActivity.this, "Please Enter PSNO and Password", Toast.LENGTH_SHORT).show();
                    }
                    else if(etpassword.getText().toString().trim().equals(etpsno.getText().toString().trim() + "_RepAdmin"))
                    {
                        progressDialog = new ProgressDialog(LoginActivity.this);
                        progressDialog.setCancelable(false);
                        progressDialog.setMessage("Loading..");
                        progressDialog.show();

                        sharedPref.putString("Role", "Admin");
                        sharedPref.putString("userPsno", etpsno.getText().toString().trim() + "_RepAdmin");
                        GoToNext();
                    }
                    else {
                        progressDialog = new ProgressDialog(LoginActivity.this);
                        progressDialog.setCancelable(false);
                        progressDialog.setMessage("Loading..");
                        progressDialog.show();


                        api = RetrofitAPIClient.getClient().create(Api.class);

                        //logincheck method inside interface api
                        Call<LoginResponse> loginResponseCall = api.loginCheck(etpsno.getText().toString(), etpassword.getText().toString());

                        loginResponseCall.enqueue(new Callback<LoginResponse>() {
                            @Override
                            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                               Log.e("response is:-",response.message());
                                if (response.isSuccessful()) {
                                    //model object
                                    LoginResponse loginResponse = response.body();
                                    Log.e("loginresponse;---", loginResponse.getResultcode());
                                    Log.e("loginresponsecode;---", String.valueOf(loginResponse.getResultcode()));
                                    String lresponse=loginResponse.getResultcode();
                                     String code="200";
                                    //if (lresponse.equals(code))
                                    //{
                                        switch (loginResponse.getResultcode()) {

                                            case "200":
                                                sharedPref.putString("Role", "NormalUser");
                                                sharedPref.putString("userName", loginResponse.getMessage());

                                                sharedPref.putString("userPsno", etpsno.getText().toString().trim());

                                                sharedPref.putString("userPassword", etpassword.getText().toString().trim());


                                                GoToNext();
                                                break;
                                            case "404":
                                                progressDialogDismiss();
                                                Toast.makeText(LoginActivity.this, getString(R.string.Login_WrongPassword), Toast.LENGTH_SHORT).show();
                                                break;
                                            case "401":
                                                progressDialogDismiss();
                                                Toast.makeText(LoginActivity.this, getString(R.string.Login_Unauthorized), Toast.LENGTH_SHORT).show();
                                                break;
                                            case "3":
                                                progressDialogDismiss();
                                                Toast.makeText(LoginActivity.this, getString(R.string.Login_Blanks), Toast.LENGTH_SHORT).show();
                                                break;
                                            default:
                                                progressDialogDismiss();
                                                Toast.makeText(LoginActivity.this, getString(R.string.HttpError), Toast.LENGTH_SHORT).show();
                                                Log.e("loginStatus",response.message());
                                                break;
                                        }
                              //  }
                                } else {
                                    progressDialogDismiss();
                                    Toast.makeText(LoginActivity.this, "Response is Unsuccessful", Toast.LENGTH_SHORT).show();
                                    Log.e("response", response.message().toString());
                                }
                            }

                            @Override
                            public void onFailure(Call<LoginResponse> call, Throwable t) {
                                progressDialogDismiss();
                                Toast.makeText(LoginActivity.this, "Error While Receiving Response", Toast.LENGTH_SHORT).show();
                                Log.e("Error", t.getMessage());
                            }
                        });
                    }
                } else {
                    Toast.makeText(LoginActivity.this, "Please Check your Network Connection", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void progressDialogDismiss() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private void GoToNext() {
        sharedPref.putBoolean("IsFirstTimeLaunch", true);
        Intent intent = new Intent(LoginActivity.this, IconListActivity.class);
        //intent.putExtra("userName", etpsno.getText().toString());
        //intent.putExtra("userPassword",etpassword.getText().toString());
        progressDialogDismiss();
        startActivity(intent);
        finish();
    }

}
