package com.example.approvalapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.approvalapp.Model.IconParam;
import com.example.approvalapp.R;

import java.util.ArrayList;

public class ListOfAppAdapter extends RecyclerView.Adapter<ListOfAppAdapter.MyViewHolder>
{

    Context context;
    ArrayList<IconParam> iconParamArrayList;

    public ListOfAppAdapter(Context context, ArrayList<IconParam> iconParamArrayList) {
        this.context = context;
        this.iconParamArrayList = iconParamArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.listresource,viewGroup,false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final IconParam icon = iconParamArrayList.get(holder.getAdapterPosition());
        holder.txtcode.setText(icon.getCode());
        holder.txtname.setText(icon.getName());
      //  holder.txtorder.setText(icon.getOrder());

        //for image get from api
      // Glide.with(context)
               // .load(icon.getIconimg())
               // .into(holder.iconimg);


    }

    @Override
    public int getItemCount() {
        return iconParamArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtcode,txtname,txtorder;
           ImageView iconimg;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
      txtcode=itemView.findViewById(R.id.code);
      txtname=itemView.findViewById(R.id.fullname);
    //  txtorder=itemView.findViewById(R.id.order);
     iconimg=itemView.findViewById(R.id.iconimage);

        }
    }


}
