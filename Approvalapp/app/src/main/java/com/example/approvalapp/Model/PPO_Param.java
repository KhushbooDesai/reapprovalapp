package com.example.approvalapp.Model;

import com.google.gson.annotations.SerializedName;

public class PPO_Param {
    @SerializedName("PPONo")
    private String PPONo;
    @SerializedName("OrderQty")
    private String OrderQty;
    @SerializedName("IC")
    private String IC;
    @SerializedName("ApproverID")
    private String ApproverID;
    @SerializedName("StatusConstantValue")
    private String StatusConstantValue;
    @SerializedName("StatusDescription")
    private String StatusDescription;
    @SerializedName("StatusName")
    private String StatusName;
    @SerializedName("Initpsno")
    private String Initpsno;
    @SerializedName("InitiatorName")
    private String  InitiatorName;

    public String getPPONo() {
        return PPONo;
    }

    public void setPPONo(String PPONo) {
        this.PPONo = PPONo;
    }

    public String getOrderQty() {
        return OrderQty;
    }

    public void setOrderQty(String orderQty) {
        OrderQty = orderQty;
    }

    public String getIC() {
        return IC;
    }

    public void setIC(String IC) {
        this.IC = IC;
    }

    public String getApproverID() {
        return ApproverID;
    }

    public void setApproverID(String approverID) {
        ApproverID = approverID;
    }

    public String getStatusConstantValue() {
        return StatusConstantValue;
    }

    public void setStatusConstantValue(String statusConstantValue) {
        StatusConstantValue = statusConstantValue;
    }

    public String getStatusDescription() {
        return StatusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        StatusDescription = statusDescription;
    }

    public String getStatusName() {
        return StatusName;
    }

    public void setStatusName(String statusName) {
        StatusName = statusName;
    }

    public String getInitpsno() {
        return Initpsno;
    }

    public void setInitpsno(String initpsno) {
        Initpsno = initpsno;
    }

    public String getInitiatorName() {
        return InitiatorName;
    }

    public void setInitiatorName(String initiatorName) {
        InitiatorName = initiatorName;
    }
}
