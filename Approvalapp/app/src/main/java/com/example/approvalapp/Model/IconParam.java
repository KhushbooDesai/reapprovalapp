package com.example.approvalapp.Model;

import com.google.gson.annotations.SerializedName;

public class IconParam {
    @SerializedName("Id")
    private String Id;

    @SerializedName("Code")
    private String Code;

    @SerializedName("Name")
    private String  name;

    @SerializedName("Icon")
    private String iconimg;

    @SerializedName("Order")
    private String order;

    @SerializedName("IsActive")
    private String  isactive;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconimg() {
        return iconimg;
    }

    public void setIconimg(String iconimg) {
        this.iconimg = iconimg;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }
}
