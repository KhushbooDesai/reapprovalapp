package com.example.approvalapp.Model;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
public class POList {

    @SerializedName("Data")
    private ArrayList<POParam> functionArrayList;

    public ArrayList<POParam> getFunctionArrayList()
    {
        return functionArrayList;
    }

    public void setUserArrayList(ArrayList<POParam> functionArrayList)
    {
        this.functionArrayList = functionArrayList;
    }



}
