package com.example.approvalapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

//import com.example.approvalapp.Adapter.Adapter;
import com.example.approvalapp.Adapter.POAdapter;
import com.example.approvalapp.Interface.Api;
import com.example.approvalapp.Model.POList;
import com.example.approvalapp.R;

import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.example.approvalapp.helper.RetrofitAPIClient;
import com.example.approvalapp.helper.SharedPref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PO_Activity extends AppCompatActivity {

    GridLayoutManager gridLayoutManager;
    RecyclerView rv_function;
//int psno = 90348357;
    LinearLayoutManager linearLayoutManager;
    ImageView logoutimg;
    TextView userPsno;
    String usernm;
    SharedPref sharedPref;

    private POAdapter functionAdapter;

    //private FunctionAdapter functionAdapter;

    SimpleDateFormat df;
    Date currentDate;
    String todayDate;

    ProgressDialog dialog;
    Api api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_p_o_);


        sharedPref = new SharedPref(PO_Activity.this);

        initView();
        initViewAction();
        initViewListener();

        //show name
        userPsno=findViewById(R.id.txtName);
        usernm=sharedPref.getString("userPsno");
        userPsno.setText("  WELCOME "+usernm);


        logoutimg.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                sharedPref.putBoolean("IsFirstTimeLaunch",false);
                sharedPref.putString("userName","");
                sharedPref.putString("PSNO","");
                sharedPref.putString("date","");
                sharedPref.putString("userPSNO","");
                sharedPref.putString("userPassword","");
                sharedPref.putString("Role","");

                CookieManager.getInstance().removeAllCookies(null);

                Intent intent=new Intent(getApplicationContext(),LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(intent);
            }
        });
    }

    /*private void parseData(List<POParam> body) {
        POAdapter adapter = new POAdapter(body);
        rv_function.setAdapter(adapter);

    }*/

    private void initView()
    {

        rv_function = findViewById(R.id.rvMain);
        rv_function.setNestedScrollingEnabled(true);
        logoutimg = findViewById(R.id.img_logout);
        gridLayoutManager = new GridLayoutManager(this,2,LinearLayoutManager.VERTICAL,false);
        rv_function.setLayoutManager(gridLayoutManager);

        //rv_function.setAdapter(functionAdapter);
    }


    private void initViewAction()
    {
        df = new SimpleDateFormat("dd/MMM/yyyy");
        currentDate = Calendar.getInstance().getTime();
        todayDate = df.format(currentDate);


        /*
        if (sharedPref.getString("date").isEmpty() || sharedPref.getString("date").equalsIgnoreCase(""))
        {
            sharedPref.putString("date",todayDate);
        }
        else
        {
            PackageInfo packageInfo = null;
            try {
                packageInfo = getPackageManager().getPackageInfo(getPackageName(),0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            String version = packageInfo.versionName;

            Log.e("version",version);
            Log.e("today",todayDate);
            Log.e("shreDate",sharedPref.getString("date"));

            api = RetrofitAPIClient.getClient().create(Api.class);

            Call<Version> versionCall = api.checkVersion(version);
            versionCall.enqueue(new Callback<Version>()
            {
                @Override
                public void onResponse(Call<Version> call, final Response<Version> response)
                {
                    if(response.isSuccessful())
                    {

                        Log.e("Version","Version from service :"+response.body().getResult());

                        if(!response.body().getResult().equalsIgnoreCase("1"))
                        {



                            AlertDialog.Builder builder=new AlertDialog.Builder(FunctionView.this);
                            builder.setTitle("New Version of APK")
                                    .setMessage("Please Install New Version of APK")
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            finishAffinity();
                                            System.exit(0);
                                        }
                                    })
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            sharedPref.putString("date",todayDate);
                                            Intent intent = new Intent(Intent.ACTION_VIEW,
                                                    Uri.parse(response.body().getResult()));
//                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                        }
                                    })
                                    .setCancelable(false)
                                    .create()
                                    .show();


                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        Toast.makeText(FunctionView.this, "Response is Unsuccessful", Toast.LENGTH_SHORT).show();
                        Log.e("response",response.message());
                    }
                }

                @Override
                public void onFailure(Call<Version> call, Throwable t)
                {

                }
            });
        }
        */


        dialog = new ProgressDialog(PO_Activity.this);
        dialog.setCancelable(false);
        dialog.setMessage("Loading..");
        dialog.show();

        String userPsno ="90348357";
        //String PSNOU = "90348357";
        api = RetrofitAPIClient.getClient().create(Api.class);

        try
        {
            //for get function
            Call<POList> callFunction = api.getallPO(userPsno);
           callFunction.enqueue(new Callback<POList>()
            {
                @Override
                public void onResponse(Call<POList> call, Response<POList> response)
                {
                    if(response.isSuccessful())
                    {
                        setAdapter(response.body());
                        forProgressDialog();
                    }
                    else
                    {
                        Toast.makeText(PO_Activity.this, "Response is Unsuccessful", Toast.LENGTH_SHORT).show();
                        forProgressDialog();
                    }
                }

                @Override
                public void onFailure(Call<POList> call, Throwable t)
                {
                    Toast.makeText(PO_Activity.this, "Error While Receiving Response", Toast.LENGTH_SHORT).show();
                    Log.e("Error",t.getMessage());
                    forProgressDialog();
                }

            });
        }
        catch (Exception e)
        {
            Log.e("Exception",e.getMessage());
            forProgressDialog();
        }
    }

    public void forProgressDialog()
    {
        if(dialog.isShowing())
        {
            dialog.dismiss();
        }
    }

    private void initViewListener()
    {
       // img_logout.setOnClickListener(this);
    }

    private void setAdapter(POList functionList)
    {
        functionAdapter =new POAdapter(this,functionList.getFunctionArrayList());
        rv_function.setAdapter(functionAdapter);
        forProgressDialog();

    }





}