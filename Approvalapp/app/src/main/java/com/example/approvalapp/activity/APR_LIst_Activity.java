package com.example.approvalapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;

import com.example.approvalapp.Interface.Api;
import com.example.approvalapp.Model.APRList;
import com.example.approvalapp.R;
import com.example.approvalapp.adapter.APR_APRList_Adapter;
import com.example.approvalapp.helper.RetrofitAPIClient;
import com.example.approvalapp.helper.SharedPref;

public class APR_LIst_Activity extends AppCompatActivity {

    Api api;
    RecyclerView recycler_view;
    SharedPref sharedPref;
    LinearLayoutManager linearLayoutManager;
    private APR_APRList_Adapter aprListAdapter;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apr_list);
        Log.e("Flag ","Came onCreate");
        initView();
        initViewAction();
    }
    public  void initView()
    {
        recycler_view = findViewById(R.id.recycler_view);
        recycler_view.setNestedScrollingEnabled(true);
        linearLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recycler_view.setLayoutManager(linearLayoutManager);
    }
    private void initViewAction()
    {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading..");
        progressDialog.show();


        api = RetrofitAPIClient.getClient().create(Api.class);

        Call<APRList> aprListData = api.GetAPRData("115577");


        aprListData.enqueue(new Callback<APRList>() {
            @Override
            public void onResponse(Call<APRList> call, Response<APRList> response) {
                if(response.isSuccessful())
                {
                    Log.e("response from service: ",response.body().getCode());


                    setAdapter(response.body());
                    Log.e("TAG", "onResponse: "+ response.body().getAPRArrayList().size());

                    progressDialogDismiss();
                }
            }

            @Override
            public void onFailure(Call<APRList> call, Throwable t) {
                Log.e("TAG", "onFailure: " );
            }
        });
    }

    private void setAdapter(APRList aprList)
    {
        aprListAdapter =new APR_APRList_Adapter(this,aprList.getAPRArrayList());
        recycler_view.setAdapter(aprListAdapter);
    }
    private void progressDialogDismiss() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
