package com.example.approvalapp.Model;

import com.google.gson.annotations.SerializedName;

public class LoginResponse
{
    @SerializedName("Result")
    private String result;

    @SerializedName("Message")
    private String message;



    @SerializedName("Code")
    private String  resultcode;


    public String getResultcode() {
        return resultcode;
    }

    public void setResultcode(String resultcode) {
        this.resultcode = resultcode;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
