package com.example.approvalapp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class IconList {
    @SerializedName("Data")
    private ArrayList<IconParam> iconArrayList;

    public ArrayList<IconParam> getIconArrayList() {
        return iconArrayList;
    }

    public void setIconArrayList(ArrayList<IconParam> iconArrayList) {
        this.iconArrayList = iconArrayList;
    }
}
