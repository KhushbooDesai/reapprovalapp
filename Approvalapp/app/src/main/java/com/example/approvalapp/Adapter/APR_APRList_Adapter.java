package com.example.approvalapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.approvalapp.Model.APRParam;
import com.example.approvalapp.R;
import com.example.approvalapp.activity.APR_Detail_Activity;

import java.util.ArrayList;

public class APR_APRList_Adapter extends RecyclerView.Adapter<APR_APRList_Adapter.APRListViewHolder> {

    Context context;
    ArrayList<APRParam> aprParamArrayList;


    public APR_APRList_Adapter(Context context,ArrayList<APRParam> aprParamArrayList){
        this.context=context;
        this.aprParamArrayList = aprParamArrayList;
    }
    @NonNull
    @Override
    public APRListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.apr_list_card,parent,false);
        APRListViewHolder aprListViewHolder = new APRListViewHolder(view);
        return aprListViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull APRListViewHolder holder, int position) {
        final APRParam APRParam = aprParamArrayList.get(holder.getAdapterPosition());
        holder.setOrderNo(APRParam.getOrderNo());
        holder.setItemAmount(APRParam.getAmount());
        holder.setRequestNo(APRParam.getRequestId());

        
    }

    @Override
    public int getItemCount() {
        return aprParamArrayList.size();
    }

    public  class APRListViewHolder extends RecyclerView.ViewHolder
    {
        private TextView txtItemOrderNo;
        private TextView txtItemRequestNo;
        private TextView txtItemAmount;
        private CardView cv_function_card;
        public APRListViewHolder(@NonNull View itemView) {
            super(itemView);

            txtItemOrderNo=itemView.findViewById(R.id.txtItemOrderNo);
            txtItemRequestNo=itemView.findViewById(R.id.txtItemRequestNo);
            txtItemAmount=itemView.findViewById(R.id.txtItemAmount);
            cv_function_card=itemView.findViewById(R.id.cv_function_card);
        }


        public void setOrderNo(String orderNo)
        {
            txtItemOrderNo.setText(orderNo);
        }
        public void setRequestNo(String requestNo)
        {
            txtItemRequestNo.setText(requestNo);
        }
        public void setItemAmount(String itemAmount)
        {
            txtItemAmount.setText(itemAmount);
        }
    }
}
