package com.example.approvalapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.approvalapp.Adapter.PPO_Adapter;
import com.example.approvalapp.Interface.Api;
import com.example.approvalapp.Model.PPO;
import com.example.approvalapp.R;
import com.example.approvalapp.helper.RetrofitAPIClient;
import com.example.approvalapp.helper.SharedPref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Ppo_ListActivity extends AppCompatActivity {
    RecyclerView rv_PPO;
    ImageView logoutimg;
    TextView userPsno;
    String usernm;
    LinearLayoutManager linearLayoutManager;

    private PPO_Adapter ppoAdapter;
    Api api;
    ProgressDialog dialog;
    SharedPref sharedPref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ppo_list);
        sharedPref = new SharedPref(Ppo_ListActivity.this);
        //sharedPref = new SharedPref(Ppo_ListActivity.this);
        //ppoAdapter = null;
        Log.e("TAG", "onCreate: PPO_LIST" );
        initView();
        initViewAction();
        /*initViewListener();*/

        //show name
        userPsno=findViewById(R.id.txtName);
        usernm=sharedPref.getString("userPsno");
        userPsno.setText("  WELCOME "+usernm);

        logoutimg.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                sharedPref.putBoolean("IsFirstTimeLaunch",false);
                sharedPref.putString("userName","");
                sharedPref.putString("PSNO","");
                sharedPref.putString("date","");
                sharedPref.putString("userPSNO","");
                sharedPref.putString("userPassword","");
                sharedPref.putString("Role","");

                CookieManager.getInstance().removeAllCookies(null);

                Intent intent=new Intent(getApplicationContext(),LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(intent);
            }
        });

    }

    private void initView() {
        rv_PPO = findViewById(R.id.rv_PPO);
        logoutimg = findViewById(R.id.img_logout);
        linearLayoutManager = new LinearLayoutManager(Ppo_ListActivity.this, LinearLayoutManager.VERTICAL, false);
        rv_PPO.setLayoutManager(linearLayoutManager);
    }

    private void initViewAction()
    {
        dialog = new ProgressDialog(Ppo_ListActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage("Loading..");
        dialog.show();

        //userPsno = sharedPref.getString("userPsno");
        api = RetrofitAPIClient.getClient().create(Api.class);

        try {
            //for ppo list
            //->Static Psno(20005945)
            Call<PPO> ppoCall = api.GetPPOData("20005945");
            ppoCall.enqueue(new Callback<PPO>() {
                @Override
                public void onResponse(Call<PPO> call, Response<PPO> response) {
                    Log.e("TAG", "onResponse: "+response.body() );
                    if (response.isSuccessful()){
                        //if(response.body()!=null){
                        setAdapter(response.body());
                        Log.e("PPO","LIST :"+response.body().getPpoArrayList().size());
                        forProgressDialog();
                        //}
                    }
                    else
                    {
                        Toast.makeText(Ppo_ListActivity.this, "Response is Unsuccessful", Toast.LENGTH_SHORT).show();
                        forProgressDialog();
                    }
                }

                @Override
                public void onFailure(Call<PPO> call, Throwable t) {
                    Toast.makeText(Ppo_ListActivity.this, "Error While Receiving Response", Toast.LENGTH_SHORT).show();
                    Log.e("Error",t.getMessage());
                    forProgressDialog();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setAdapter(PPO ppoList)
    {
        ppoAdapter =new PPO_Adapter(getApplicationContext(),ppoList.getPpoArrayList());
        rv_PPO.setAdapter(ppoAdapter);
        //forProgressDialog();
    }

    public void forProgressDialog()
    {
        if(dialog.isShowing())
        {
            dialog.dismiss();
        }
    }

    private void initViewListener() {
        logoutimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.img_logout:
                /*sharedPref.putBoolean("IsFirstTimeLaunch",false);
                sharedPref.putString("userName","");
                sharedPref.putString("PSNO","");
                sharedPref.putString("date","");
                sharedPref.putString("userPSNO","");
                sharedPref.putString("userPassword","");
                sharedPref.putString("Role","");*/

                //CookieManager.getInstance().removeAllCookies(null);

                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        break;
                }
            }
        });
    }
}
