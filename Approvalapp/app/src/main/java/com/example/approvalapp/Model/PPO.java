package com.example.approvalapp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PPO {
    @SerializedName("Result")
    String result;
    @SerializedName("Message")
    String Message;
    @SerializedName("Code")
    String Code;
    @SerializedName("Data")
    private ArrayList<PPO_Param> ppoArrayList;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public ArrayList<PPO_Param> getPpoArrayList() {
        return ppoArrayList;
    }

    public void setPpoArrayList(ArrayList<PPO_Param> ppoArrayList) {
        this.ppoArrayList = ppoArrayList;
    }
}
